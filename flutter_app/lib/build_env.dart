import 'package:meta/meta.dart';

enum BuildFlavor { prod, dev, stagging, ci }

BuildEnvironment get env => _env;
BuildEnvironment _env;
/*
   In the main dart file can expose the env
   vars by importing build_env while
   the individual vars are actually set in the
   main_stagging
   main_prud
   main_dev
   main_ci

   files

   in your .gitignore
   you set to ignore the
   files of

   main_stagging
   main_dev
   main_prod
   main_ci

   in the lib folder

   Works with intellij, android studio, VS and hopefully Xcode and of course the
   command line with the -t target flag pointin to the main_dev for example path

   Modified from this stackoverflow answer thread:

   https://stackoverflow.com/questions/47438564/how-do-i-build-different-versions-of-my-flutter-app-for-qa-dev-prod
 */
class BuildEnvironment {
  final BuildFlavor flavor;

  BuildEnvironment._init({this.flavor});

  static void init({@required flavor}) =>
      _env ??= BuildEnvironment._init(flavor: flavor);
}